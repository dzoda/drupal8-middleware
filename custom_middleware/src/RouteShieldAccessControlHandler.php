<?php

namespace Drupal\custom_middleware;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Route shield entity.
 *
 * @see \Drupal\custom_middleware\Entity\RouteShield.
 */
class RouteShieldAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\custom_middleware\Entity\RouteShieldInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished route shield entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published route shield entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit route shield entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete route shield entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add route shield entities');
  }

}
