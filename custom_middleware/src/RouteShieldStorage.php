<?php

namespace Drupal\custom_middleware;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\custom_middleware\Entity\RouteShieldInterface;

/**
 * Defines the storage handler class for Route shield entities.
 *
 * This extends the base storage class, adding required special handling for
 * Route shield entities.
 *
 * @ingroup custom_middleware
 */
class RouteShieldStorage extends SqlContentEntityStorage implements RouteShieldStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function revisionIds(RouteShieldInterface $entity) {
    return $this->database->query(
      'SELECT vid FROM {route_shield_revision} WHERE id=:id ORDER BY vid',
      [':id' => $entity->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function userRevisionIds(AccountInterface $account) {
    return $this->database->query(
      'SELECT vid FROM {route_shield_field_revision} WHERE uid = :uid ORDER BY vid',
      [':uid' => $account->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function countDefaultLanguageRevisions(RouteShieldInterface $entity) {
    return $this->database->query('SELECT COUNT(*) FROM {route_shield_field_revision} WHERE id = :id AND default_langcode = 1', [':id' => $entity->id()])
      ->fetchField();
  }

  /**
   * {@inheritdoc}
   */
  public function clearRevisionsLanguage(LanguageInterface $language) {
    return $this->database->update('route_shield_revision')
      ->fields(['langcode' => LanguageInterface::LANGCODE_NOT_SPECIFIED])
      ->condition('langcode', $language->getId())
      ->execute();
  }

}
