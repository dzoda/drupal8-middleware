<?php
/**
 * @file
 *   Contains \Drupal\shield\ShieldMiddleware.
 */

namespace Drupal\custom_middleware;

use Drupal;
use Drupal\Component\Utility\Crypt;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\HttpKernelInterface;

/**
 * Middleware for the shield module.
 */
class CustomShieldMiddleware implements HttpKernelInterface {

  /**
   * The decorated kernel.
   *
   * @var \Symfony\Component\HttpKernel\HttpKernelInterface
   */
  protected $httpKernel;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Constructs a BanMiddleware object.
   *
   * @param \Symfony\Component\HttpKernel\HttpKernelInterface $http_kernel
   *   The decorated kernel.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory.
   */
  public function __construct(HttpKernelInterface $http_kernel, ConfigFactoryInterface $config_factory) {
    $this->httpKernel = $http_kernel;
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public function handle(Request $request, $type = self::MASTER_REQUEST, $catch = TRUE) {

    $check = $this->checkCurrentRoute($request);
    if($request->getPathInfo() != $check['route']) {
      return $this->httpKernel->handle($request, $type, $catch);
    }
    $user = $check['username'];
    $pass = $check['password'];
    if ($request->server->has('PHP_AUTH_USER') && $request->server->has('PHP_AUTH_PW')) {
        $input_user = $request->server->get('PHP_AUTH_USER');
        $input_pass = $request->server->get('PHP_AUTH_PW');
      }
      elseif ($request->server->has('HTTP_AUTHORIZATION')) {
        list($input_user, $input_pass) = explode(':', base64_decode(substr($request->server->get('HTTP_AUTHORIZATION'), 6)), 2);
      }
      elseif ($request->server->has('REDIRECT_HTTP_AUTHORIZATION')) {
        list($input_user, $input_pass) = explode(':', base64_decode(substr($request->server->get('REDIRECT_HTTP_AUTHORIZATION'), 6)), 2);
      }

      if (isset($input_user) && $input_user === $user && Crypt::hashEquals($pass, $input_pass)) {
        return $this->httpKernel->handle($request, $type, $catch);
      }
    $response = new Response();
    $response->headers->add([
      'WWW-Authenticate' => 'Basic realm="' . strtr('Protected area!', [
          '[user]' => $user,
          '[password]' => $pass,
        ]) . '"',
    ]);
    $response->setStatusCode(401);
    return $response;
  }


  /**
   * {@inheritdoc}
   */
  public function checkCurrentRoute(Request $request) {
    $data = [];
    $path = $request->getPathInfo();
    $storage = Drupal::entityTypeManager()->getStorage('route_shield');
    $routes = $storage->loadMultiple();

    foreach ($routes as $key => $value) {
      $route = $value->get('route')->first()->getString();
      if((!empty($route)) && $route == $path) {
        $shield_username = $value->get('shield_username')->first()->getString();
        $shield_password = $value->get('shield_password')->first()->getString();
        $data = [
          'route' => $path,
          'username' => ($shield_username) ? $shield_username : '',
          'password' => ($shield_password) ? $shield_password : '',
        ];
      }
    }
    return $data;
  }
}
