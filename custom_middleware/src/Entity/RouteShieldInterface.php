<?php

namespace Drupal\custom_middleware\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Route shield entities.
 *
 * @ingroup custom_middleware
 */
interface RouteShieldInterface extends ContentEntityInterface, RevisionLogInterface, EntityChangedInterface, EntityOwnerInterface {

  // Add get/set methods for your configuration properties here.

  /**
   * Gets the Route shield name.
   *
   * @return string
   *   Name of the Route shield.
   */
  public function getName();

  /**
   * Sets the Route shield name.
   *
   * @param string $name
   *   The Route shield name.
   *
   * @return \Drupal\custom_middleware\Entity\RouteShieldInterface
   *   The called Route shield entity.
   */
  public function setName($name);

  /**
   * Gets the Route shield name.
   *
   * @return string
   *   Name of the Route shield.
   */
  public function getRoute();

  /**
   * Sets the Route shield name.
   *
   * @param string $name
   *   The Route shield name.
   *
   * @return \Drupal\custom_middleware\Entity\RouteShieldInterface
   *   The called Route shield entity.
   */
  public function setRoute($name);

  /**
   * Gets the Route shield name.
   *
   * @return string
   *   Name of the Route shield.
   */
  public function getShieldUsername();

  /**
   * Sets the Route shield name.
   *
   * @param string $name
   *   The Route shield name.
   *
   * @return \Drupal\custom_middleware\Entity\RouteShieldInterface
   *   The called Route shield entity.
   */
  public function setShieldUsername($name);

  /**
   * Gets the Route shield name.
   *
   * @return string
   *   Name of the Route shield.
   */
  public function getShieldPassword();

  /**
   * Sets the Route shield name.
   *
   * @param string $name
   *   The Route shield name.
   *
   * @return \Drupal\custom_middleware\Entity\RouteShieldInterface
   *   The called Route shield entity.
   */
  public function setShieldPassword($name);

  /**
   * Gets the Route shield creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Route shield.
   */
  public function getCreatedTime();

  /**
   * Sets the Route shield creation timestamp.
   *
   * @param int $timestamp
   *   The Route shield creation timestamp.
   *
   * @return \Drupal\custom_middleware\Entity\RouteShieldInterface
   *   The called Route shield entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Route shield published status indicator.
   *
   * Unpublished Route shield are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Route shield is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Route shield.
   *
   * @param bool $published
   *   TRUE to set this Route shield to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\custom_middleware\Entity\RouteShieldInterface
   *   The called Route shield entity.
   */
  public function setPublished($published);

  /**
   * Gets the Route shield revision creation timestamp.
   *
   * @return int
   *   The UNIX timestamp of when this revision was created.
   */
  public function getRevisionCreationTime();

  /**
   * Sets the Route shield revision creation timestamp.
   *
   * @param int $timestamp
   *   The UNIX timestamp of when this revision was created.
   *
   * @return \Drupal\custom_middleware\Entity\RouteShieldInterface
   *   The called Route shield entity.
   */
  public function setRevisionCreationTime($timestamp);

  /**
   * Gets the Route shield revision author.
   *
   * @return \Drupal\user\UserInterface
   *   The user entity for the revision author.
   */
  public function getRevisionUser();

  /**
   * Sets the Route shield revision author.
   *
   * @param int $uid
   *   The user ID of the revision author.
   *
   * @return \Drupal\custom_middleware\Entity\RouteShieldInterface
   *   The called Route shield entity.
   */
  public function setRevisionUserId($uid);

}
