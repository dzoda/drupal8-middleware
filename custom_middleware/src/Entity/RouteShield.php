<?php

namespace Drupal\custom_middleware\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\RevisionableContentEntityBase;
use Drupal\Core\Entity\RevisionableInterface;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\UserInterface;

/**
 * Defines the Route shield entity.
 *
 * @ingroup custom_middleware
 *
 * @ContentEntityType(
 *   id = "route_shield",
 *   label = @Translation("Route shield"),
 *   handlers = {
 *     "storage" = "Drupal\custom_middleware\RouteShieldStorage",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\custom_middleware\RouteShieldListBuilder",
 *     "views_data" = "Drupal\custom_middleware\Entity\RouteShieldViewsData",
 *     "translation" = "Drupal\custom_middleware\RouteShieldTranslationHandler",
 *
 *     "form" = {
 *       "default" = "Drupal\custom_middleware\Form\RouteShieldForm",
 *       "add" = "Drupal\custom_middleware\Form\RouteShieldForm",
 *       "edit" = "Drupal\custom_middleware\Form\RouteShieldForm",
 *       "delete" = "Drupal\custom_middleware\Form\RouteShieldDeleteForm",
 *     },
 *     "access" = "Drupal\custom_middleware\RouteShieldAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\custom_middleware\RouteShieldHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "route_shield",
 *   data_table = "route_shield_field_data",
 *   revision_table = "route_shield_revision",
 *   revision_data_table = "route_shield_field_revision",
 *   translatable = TRUE,
 *   admin_permission = "administer route shield entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "vid",
 *     "label" = "name",
 *     "route" = "route",
 *     "shield_username" = "shield_username",
 *     "shield_password" = "shield_password",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *     "status" = "status",
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/route_shield/{route_shield}",
 *     "add-form" = "/admin/structure/route_shield/add",
 *     "edit-form" = "/admin/structure/route_shield/{route_shield}/edit",
 *     "delete-form" = "/admin/structure/route_shield/{route_shield}/delete",
 *     "version-history" = "/admin/structure/route_shield/{route_shield}/revisions",
 *     "revision" = "/admin/structure/route_shield/{route_shield}/revisions/{route_shield_revision}/view",
 *     "revision_revert" = "/admin/structure/route_shield/{route_shield}/revisions/{route_shield_revision}/revert",
 *     "revision_delete" = "/admin/structure/route_shield/{route_shield}/revisions/{route_shield_revision}/delete",
 *     "translation_revert" = "/admin/structure/route_shield/{route_shield}/revisions/{route_shield_revision}/revert/{langcode}",
 *     "collection" = "/admin/structure/route_shield",
 *   },
 *   field_ui_base_route = "route_shield.settings"
 * )
 */
class RouteShield extends RevisionableContentEntityBase implements RouteShieldInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'user_id' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function urlRouteParameters($rel) {
    $uri_route_parameters = parent::urlRouteParameters($rel);

    if ($rel === 'revision_revert' && $this instanceof RevisionableInterface) {
      $uri_route_parameters[$this->getEntityTypeId() . '_revision'] = $this->getRevisionId();
    }
    elseif ($rel === 'revision_delete' && $this instanceof RevisionableInterface) {
      $uri_route_parameters[$this->getEntityTypeId() . '_revision'] = $this->getRevisionId();
    }

    return $uri_route_parameters;
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);

    foreach (array_keys($this->getTranslationLanguages()) as $langcode) {
      $translation = $this->getTranslation($langcode);

      // If no owner has been set explicitly, make the anonymous user the owner.
      if (!$translation->getOwner()) {
        $translation->setOwnerId(0);
      }
    }

    // If no revision author has been set explicitly, make the route_shield owner the
    // revision author.
    if (!$this->getRevisionUser()) {
      $this->setRevisionUserId($this->getOwnerId());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getRoute() {
    return $this->get('route')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setRoute($name) {
    $this->set('route', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getShieldUsername() {
    return $this->get('shield_username')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setShieldUsername($name) {
    $this->set('shield_username', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getShieldPassword() {
    return $this->get('shield_password')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setShieldPassword($name) {
    $this->set('shield_password', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isPublished() {
    return (bool) $this->getEntityKey('status');
  }

  /**
   * {@inheritdoc}
   */
  public function setPublished($published) {
    $this->set('status', $published ? TRUE : FALSE);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('The user ID of author of the Route shield entity.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the Route shield entity.'))
      ->setRevisionable(TRUE)
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['route'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Route URL path'))
      ->setDescription(t('Route URL path.'))
      ->setRevisionable(TRUE)
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['shield_username'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Shield Username'))
      ->setDescription(t('Shield Username.'))
      ->setRevisionable(TRUE)
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['shield_password'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Shield Password'))
      ->setDescription(t('Shield Password.'))
      ->setRevisionable(TRUE)
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Publishing status'))
      ->setDescription(t('A boolean indicating whether the Route shield is published.'))
      ->setRevisionable(TRUE)
      ->setDefaultValue(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => -3,
      ]);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    $fields['revision_translation_affected'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Revision translation affected'))
      ->setDescription(t('Indicates if the last edit of a translation belongs to current revision.'))
      ->setReadOnly(TRUE)
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE);

    return $fields;
  }

}
