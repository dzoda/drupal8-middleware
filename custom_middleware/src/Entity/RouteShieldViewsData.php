<?php

namespace Drupal\custom_middleware\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Route shield entities.
 */
class RouteShieldViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    // Additional information for Views integration, such as table joins, can be
    // put here.

    return $data;
  }

}
