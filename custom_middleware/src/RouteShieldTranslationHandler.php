<?php

namespace Drupal\custom_middleware;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for route_shield.
 */
class RouteShieldTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.

}
