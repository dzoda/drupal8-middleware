<?php

namespace Drupal\custom_middleware;

use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\custom_middleware\Entity\RouteShieldInterface;

/**
 * Defines the storage handler class for Route shield entities.
 *
 * This extends the base storage class, adding required special handling for
 * Route shield entities.
 *
 * @ingroup custom_middleware
 */
interface RouteShieldStorageInterface extends ContentEntityStorageInterface {

  /**
   * Gets a list of Route shield revision IDs for a specific Route shield.
   *
   * @param \Drupal\custom_middleware\Entity\RouteShieldInterface $entity
   *   The Route shield entity.
   *
   * @return int[]
   *   Route shield revision IDs (in ascending order).
   */
  public function revisionIds(RouteShieldInterface $entity);

  /**
   * Gets a list of revision IDs having a given user as Route shield author.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user entity.
   *
   * @return int[]
   *   Route shield revision IDs (in ascending order).
   */
  public function userRevisionIds(AccountInterface $account);

  /**
   * Counts the number of revisions in the default language.
   *
   * @param \Drupal\custom_middleware\Entity\RouteShieldInterface $entity
   *   The Route shield entity.
   *
   * @return int
   *   The number of revisions in the default language.
   */
  public function countDefaultLanguageRevisions(RouteShieldInterface $entity);

  /**
   * Unsets the language for all Route shield with the given language.
   *
   * @param \Drupal\Core\Language\LanguageInterface $language
   *   The language object.
   */
  public function clearRevisionsLanguage(LanguageInterface $language);

}
