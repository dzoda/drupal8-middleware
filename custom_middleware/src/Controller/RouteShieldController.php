<?php

namespace Drupal\custom_middleware\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Url;
use Drupal\custom_middleware\Entity\RouteShieldInterface;

/**
 * Class RouteShieldController.
 *
 *  Returns responses for Route shield routes.
 */
class RouteShieldController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * Displays a Route shield  revision.
   *
   * @param int $route_shield_revision
   *   The Route shield  revision ID.
   *
   * @return array
   *   An array suitable for drupal_render().
   */
  public function revisionShow($route_shield_revision) {
    $route_shield = $this->entityManager()->getStorage('route_shield')->loadRevision($route_shield_revision);
    $view_builder = $this->entityManager()->getViewBuilder('route_shield');

    return $view_builder->view($route_shield);
  }

  /**
   * Page title callback for a Route shield  revision.
   *
   * @param int $route_shield_revision
   *   The Route shield  revision ID.
   *
   * @return string
   *   The page title.
   */
  public function revisionPageTitle($route_shield_revision) {
    $route_shield = $this->entityManager()->getStorage('route_shield')->loadRevision($route_shield_revision);
    return $this->t('Revision of %title from %date', ['%title' => $route_shield->label(), '%date' => format_date($route_shield->getRevisionCreationTime())]);
  }

  /**
   * Generates an overview table of older revisions of a Route shield .
   *
   * @param \Drupal\custom_middleware\Entity\RouteShieldInterface $route_shield
   *   A Route shield  object.
   *
   * @return array
   *   An array as expected by drupal_render().
   */
  public function revisionOverview(RouteShieldInterface $route_shield) {
    $account = $this->currentUser();
    $langcode = $route_shield->language()->getId();
    $langname = $route_shield->language()->getName();
    $languages = $route_shield->getTranslationLanguages();
    $has_translations = (count($languages) > 1);
    $route_shield_storage = $this->entityManager()->getStorage('route_shield');

    $build['#title'] = $has_translations ? $this->t('@langname revisions for %title', ['@langname' => $langname, '%title' => $route_shield->label()]) : $this->t('Revisions for %title', ['%title' => $route_shield->label()]);
    $header = [$this->t('Revision'), $this->t('Operations')];

    $revert_permission = (($account->hasPermission("revert all route shield revisions") || $account->hasPermission('administer route shield entities')));
    $delete_permission = (($account->hasPermission("delete all route shield revisions") || $account->hasPermission('administer route shield entities')));

    $rows = [];

    $vids = $route_shield_storage->revisionIds($route_shield);

    $latest_revision = TRUE;

    foreach (array_reverse($vids) as $vid) {
      /** @var \Drupal\custom_middleware\RouteShieldInterface $revision */
      $revision = $route_shield_storage->loadRevision($vid);
      // Only show revisions that are affected by the language that is being
      // displayed.
      if ($revision->hasTranslation($langcode) && $revision->getTranslation($langcode)->isRevisionTranslationAffected()) {
        $username = [
          '#theme' => 'username',
          '#account' => $revision->getRevisionUser(),
        ];

        // Use revision link to link to revisions that are not active.
        $date = \Drupal::service('date.formatter')->format($revision->getRevisionCreationTime(), 'short');
        if ($vid != $route_shield->getRevisionId()) {
          $link = $this->l($date, new Url('entity.route_shield.revision', ['route_shield' => $route_shield->id(), 'route_shield_revision' => $vid]));
        }
        else {
          $link = $route_shield->link($date);
        }

        $row = [];
        $column = [
          'data' => [
            '#type' => 'inline_template',
            '#template' => '{% trans %}{{ date }} by {{ username }}{% endtrans %}{% if message %}<p class="revision-log">{{ message }}</p>{% endif %}',
            '#context' => [
              'date' => $link,
              'username' => \Drupal::service('renderer')->renderPlain($username),
              'message' => ['#markup' => $revision->getRevisionLogMessage(), '#allowed_tags' => Xss::getHtmlTagList()],
            ],
          ],
        ];
        $row[] = $column;

        if ($latest_revision) {
          $row[] = [
            'data' => [
              '#prefix' => '<em>',
              '#markup' => $this->t('Current revision'),
              '#suffix' => '</em>',
            ],
          ];
          foreach ($row as &$current) {
            $current['class'] = ['revision-current'];
          }
          $latest_revision = FALSE;
        }
        else {
          $links = [];
          if ($revert_permission) {
            $links['revert'] = [
              'title' => $this->t('Revert'),
              'url' => $has_translations ?
              Url::fromRoute('entity.route_shield.translation_revert', ['route_shield' => $route_shield->id(), 'route_shield_revision' => $vid, 'langcode' => $langcode]) :
              Url::fromRoute('entity.route_shield.revision_revert', ['route_shield' => $route_shield->id(), 'route_shield_revision' => $vid]),
            ];
          }

          if ($delete_permission) {
            $links['delete'] = [
              'title' => $this->t('Delete'),
              'url' => Url::fromRoute('entity.route_shield.revision_delete', ['route_shield' => $route_shield->id(), 'route_shield_revision' => $vid]),
            ];
          }

          $row[] = [
            'data' => [
              '#type' => 'operations',
              '#links' => $links,
            ],
          ];
        }

        $rows[] = $row;
      }
    }

    $build['route_shield_revisions_table'] = [
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => $header,
    ];

    return $build;
  }

}
