<?php

namespace Drupal\custom_middleware\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Route shield entities.
 *
 * @ingroup custom_middleware
 */
class RouteShieldDeleteForm extends ContentEntityDeleteForm {


}
