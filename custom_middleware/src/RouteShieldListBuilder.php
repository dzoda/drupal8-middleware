<?php

namespace Drupal\custom_middleware;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of Route shield entities.
 *
 * @ingroup custom_middleware
 */
class RouteShieldListBuilder extends EntityListBuilder {


  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Route shield ID');
    $header['name'] = $this->t('Name');
    $header['route'] = $this->t('Route');
    $header['shield_username'] = $this->t('Shield Username');
    $header['shield_password'] = $this->t('Shield Password');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var $entity \Drupal\custom_middleware\Entity\RouteShield */
    $row['id'] = $entity->id();
    $row['name'] = Link::createFromRoute(
      $entity->label(),
      'entity.route_shield.edit_form',
      ['route_shield' => $entity->id()]
    );
    $row['route'] = $entity->getRoute();
    $row['shield_username'] = $entity->getShieldUsername();
    $row['shield_password'] = $entity->getShieldPassword();
    return $row + parent::buildRow($entity);
  }

}
