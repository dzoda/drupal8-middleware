<?php

/**
 * @file
 * Contains route_shield.page.inc.
 *
 * Page callback for Route shield entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Route shield templates.
 *
 * Default template: route_shield.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_route_shield(array &$variables) {
  // Fetch RouteShield Entity Object.
  $route_shield = $variables['elements']['#route_shield'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
